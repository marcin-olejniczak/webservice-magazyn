/**
 * MagazynServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package magazyn.service;

public class MagazynServiceLocator extends org.apache.axis.client.Service implements magazyn.service.MagazynService {

    public MagazynServiceLocator() {
    }


    public MagazynServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public MagazynServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for Magazyn
    private java.lang.String Magazyn_address = "http://localhost:8080/magazyn/services/Magazyn";

    public java.lang.String getMagazynAddress() {
        return Magazyn_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String MagazynWSDDServiceName = "Magazyn";

    public java.lang.String getMagazynWSDDServiceName() {
        return MagazynWSDDServiceName;
    }

    public void setMagazynWSDDServiceName(java.lang.String name) {
        MagazynWSDDServiceName = name;
    }

    public magazyn.service.Magazyn getMagazyn() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(Magazyn_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getMagazyn(endpoint);
    }

    public magazyn.service.Magazyn getMagazyn(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            magazyn.service.MagazynSoapBindingStub _stub = new magazyn.service.MagazynSoapBindingStub(portAddress, this);
            _stub.setPortName(getMagazynWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setMagazynEndpointAddress(java.lang.String address) {
        Magazyn_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (magazyn.service.Magazyn.class.isAssignableFrom(serviceEndpointInterface)) {
                magazyn.service.MagazynSoapBindingStub _stub = new magazyn.service.MagazynSoapBindingStub(new java.net.URL(Magazyn_address), this);
                _stub.setPortName(getMagazynWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("Magazyn".equals(inputPortName)) {
            return getMagazyn();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://service.magazyn", "MagazynService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://service.magazyn", "Magazyn"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("Magazyn".equals(portName)) {
            setMagazynEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
