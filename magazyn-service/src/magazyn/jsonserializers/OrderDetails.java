package magazyn.jsonserializers;

import java.util.ArrayList;
import java.util.Set;

import magazyn.model.Sklepy;
import magazyn.model.Uzytkownicy;
import magazyn.model.Zamowienia;

public class OrderDetails{
	private int id;
	private UserDetails user;
	private ShopDetails shop;
	private Set<OrderDetailDetails> order_details;
	public OrderDetails(Zamowienia order, UserDetails user, ShopDetails shop, Set<OrderDetailDetails> order_details){
		this.setId(order.getId());
		this.setUser(user);
		this.setShop(shop);
		this.setOrder_details(order_details);
	}
	public UserDetails getUser() {
		return user;
	}
	public void setUser(UserDetails user) {
		this.user = user;
	}
	public ShopDetails getShop() {
		return shop;
	}
	public void setShop(ShopDetails shop) {
		this.shop = shop;
	}
	public Set<OrderDetailDetails> getOrder_details() {
		return order_details;
	}
	public void setOrder_details(Set<OrderDetailDetails> order_details) {
		this.order_details = order_details;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}