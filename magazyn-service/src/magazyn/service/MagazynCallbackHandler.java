
/**
 * MagazynCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:47:34 BST)
 */

    package magazyn.service;

    /**
     *  MagazynCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class MagazynCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public MagazynCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public MagazynCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for ping method
            * override this method for handling normal response from ping operation
            */
           public void receiveResultping(
                    magazyn.service.MagazynStub.PingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from ping operation
           */
            public void receiveErrorping(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for findProducts method
            * override this method for handling normal response from findProducts operation
            */
           public void receiveResultfindProducts(
                    magazyn.service.MagazynStub.FindProductsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from findProducts operation
           */
            public void receiveErrorfindProducts(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addOrder method
            * override this method for handling normal response from addOrder operation
            */
           public void receiveResultaddOrder(
                    magazyn.service.MagazynStub.AddOrderResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addOrder operation
           */
            public void receiveErroraddOrder(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for productDetails method
            * override this method for handling normal response from productDetails operation
            */
           public void receiveResultproductDetails(
                    magazyn.service.MagazynStub.ProductDetailsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from productDetails operation
           */
            public void receiveErrorproductDetails(java.lang.Exception e) {
            }
                


    }
    