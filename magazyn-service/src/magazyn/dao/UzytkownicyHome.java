package magazyn.dao;
// Generated Dec 31, 2016 1:09:39 PM by Hibernate Tools 5.2.0.CR1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import magazyn.model.Uzytkownicy;

import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class Uzytkownicy.
 * @see magazyn.model.Uzytkownicy
 * @author Hibernate Tools
 */
public class UzytkownicyHome {

	private static final Log log = LogFactory.getLog(UzytkownicyHome.class);

	private final SessionFactory sessionFactory = GetSessionFactory.getSessionFactory();
	private Session current_session = sessionFactory.getCurrentSession();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(Uzytkownicy transientInstance) {
		log.debug("persisting Uzytkownicy instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Uzytkownicy instance) {
		log.debug("attaching dirty Uzytkownicy instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Uzytkownicy instance) {
		log.debug("attaching clean Uzytkownicy instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Uzytkownicy persistentInstance) {
		log.debug("deleting Uzytkownicy instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Uzytkownicy merge(Uzytkownicy detachedInstance) {
		log.debug("merging Uzytkownicy instance");
		try {
			Uzytkownicy result = (Uzytkownicy) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Uzytkownicy findById(int id) {
		log.debug("getting Uzytkownicy instance with id: " + id);
		try {
			Uzytkownicy instance = (Uzytkownicy) sessionFactory.getCurrentSession().get("magazyn.model.Uzytkownicy",
					id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Uzytkownicy> findByExample(Uzytkownicy instance) {
		log.debug("finding Uzytkownicy instance by example");
		try {
			List<Uzytkownicy> results = (List<Uzytkownicy>) sessionFactory.getCurrentSession()
					.createCriteria("magazyn.model.Uzytkownicy").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
