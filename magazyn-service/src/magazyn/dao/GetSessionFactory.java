package magazyn.dao;

import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class GetSessionFactory {
	
	private static final Log log = LogFactory.getLog(UzytkownicySklepowHome.class);
	
	private static SessionFactory session_factory = null;

	protected static SessionFactory buildSessionFactory() {
		try {
			return (SessionFactory) new Configuration().configure(
                    "hibernate.cfg.xml")
                    .buildSessionFactory();
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}
	
	protected GetSessionFactory(){
		
	}

	public static SessionFactory getSessionFactory(){
		if(session_factory == null){
			session_factory = buildSessionFactory();
		}
		return session_factory;
	}
}

/*
public class ClassicSingleton {
	private static ClassicSingleton instance = null;
	protected ClassicSingleton() {
	   // Exists only to defeat instantiation.
	}
	public static ClassicSingleton getInstance() {
	   if(instance == null) {
	      instance = new ClassicSingleton();
	   }
	   return instance;
	}
}
*/