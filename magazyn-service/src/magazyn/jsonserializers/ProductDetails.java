package magazyn.jsonserializers;

import magazyn.model.Produkty;
import magazyn.model.TypyProduktow;

public class ProductDetails {
	public int id;
	private String name;
	private String description;
	private String unit;
	private Integer amount;
	private Integer price;
	private Integer code;
	private Integer manufacturer_id;
	
	public ProductDetails(Produkty product){
		this.id = product.getId();
		this.setName(product.getNazwa());
		this.setDescription(product.getOpis());
		this.setUnit(product.getJednostkaMiary());
		this.setAmount(product.getIlosc());
		this.setPrice(product.getCena());
		this.setCode(product.getKodProduktu());
		this.setManufacturer_id(product.getProducentId());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Integer getManufacturer_id() {
		return manufacturer_id;
	}

	public void setManufacturer_id(Integer manufacturer_id) {
		this.manufacturer_id = manufacturer_id;
	};
}
