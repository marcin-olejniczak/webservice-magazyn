package magazyn.dao;
// Generated Dec 31, 2016 1:09:39 PM by Hibernate Tools 5.2.0.CR1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import magazyn.model.SzczegolyDostawy;

import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class SzczegolyDostawy.
 * @see magazyn.model.SzczegolyDostawy
 * @author Hibernate Tools
 */
public class SzczegolyDostawyHome {

	private static final Log log = LogFactory.getLog(SzczegolyDostawyHome.class);

	private final SessionFactory sessionFactory = GetSessionFactory.getSessionFactory();
	private Session current_session = sessionFactory.openSession();

	public void persist(SzczegolyDostawy transientInstance) {
		log.debug("persisting SzczegolyDostawy instance");
		try {
			current_session.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(SzczegolyDostawy instance) {
		log.debug("attaching dirty SzczegolyDostawy instance");
		try {
			current_session.saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(SzczegolyDostawy instance) {
		log.debug("attaching clean SzczegolyDostawy instance");
		try {
			current_session.lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(SzczegolyDostawy persistentInstance) {
		log.debug("deleting SzczegolyDostawy instance");
		try {
			current_session.delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public SzczegolyDostawy merge(SzczegolyDostawy detachedInstance) {
		log.debug("merging SzczegolyDostawy instance");
		try {
			SzczegolyDostawy result = (SzczegolyDostawy) current_session.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public SzczegolyDostawy findById(int id) {
		log.debug("getting SzczegolyDostawy instance with id: " + id);
		try {
			SzczegolyDostawy instance = (SzczegolyDostawy) current_session
					.get("magazyn.model.SzczegolyDostawy", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<SzczegolyDostawy> findByExample(SzczegolyDostawy instance) {
		log.debug("finding SzczegolyDostawy instance by example");
		try {
			List<SzczegolyDostawy> results = (List<SzczegolyDostawy>) current_session
					.createCriteria("magazyn.model.SzczegolyDostawy").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
