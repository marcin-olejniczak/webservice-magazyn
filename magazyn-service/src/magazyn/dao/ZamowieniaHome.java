package magazyn.dao;
// Generated Dec 31, 2016 1:09:39 PM by Hibernate Tools 5.2.0.CR1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import magazyn.model.Zamowienia;

import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class Zamowienia.
 * @see magazyn.model.Zamowienia
 * @author Hibernate Tools
 */
public class ZamowieniaHome {

	private static final Log log = LogFactory.getLog(ZamowieniaHome.class);

	private final SessionFactory sessionFactory = GetSessionFactory.getSessionFactory();
	private Session current_session = sessionFactory.openSession();
	

	public void persist(Zamowienia transientInstance) {
		Transaction tr = current_session.beginTransaction();
		log.debug("persisting Zamowienia instance");
		try {
			current_session.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			tr.rollback();
			log.error("persist failed", re);
			throw re;
		}
		tr.commit();
	}

	public void attachDirty(Zamowienia instance) {
		log.debug("attaching dirty Zamowienia instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Zamowienia instance) {
		log.debug("attaching clean Zamowienia instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Zamowienia persistentInstance) {
		log.debug("deleting Zamowienia instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Zamowienia merge(Zamowienia detachedInstance) {
		log.debug("merging Zamowienia instance");
		try {
			Zamowienia result = (Zamowienia) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Zamowienia findById(int id) {
		Transaction tr = current_session.beginTransaction();
		log.debug("getting Zamowienia instance with id: " + id);
		try {
			Zamowienia instance = (Zamowienia) current_session.get("magazyn.model.Zamowienia", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			tr.commit();
			return instance;
		} catch (RuntimeException re) {
			tr.rollback();
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Zamowienia> findByExample(Zamowienia instance) {
		log.debug("finding Zamowienia instance by example");
		try {
			List<Zamowienia> results = (List<Zamowienia>) sessionFactory.getCurrentSession()
					.createCriteria("magazyn.model.Zamowienia").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
