package magazyn_axis2;

import javax.jws.WebService;

@WebService(targetNamespace = "http://magazyn_axis2/", portName = "TestPort", serviceName = "TestService")
public class Test {
	public String ping(){
		return "ok";
	}
}

