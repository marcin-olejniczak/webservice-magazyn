package magazyn.dao;
// Generated Dec 31, 2016 1:09:39 PM by Hibernate Tools 5.2.0.CR1

import java.util.List;
import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import magazyn.model.SzczegolyZamowienia;

import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class SzczegolyZamowienia.
 * @see magazyn.model.SzczegolyZamowienia
 * @author Hibernate Tools
 */
public class SzczegolyZamowieniaHome {

	private static final Log log = LogFactory.getLog(SzczegolyZamowieniaHome.class);

	private final SessionFactory sessionFactory = GetSessionFactory.getSessionFactory();
	private Session current_session = sessionFactory.getCurrentSession();

	public void persist(SzczegolyZamowienia transientInstance) {
		log.debug("persisting SzczegolyZamowienia instance");
		Session current_session = sessionFactory.getCurrentSession();
		Transaction tx = current_session.beginTransaction();
		try {
			current_session.persist(transientInstance);
			log.debug("persist successful");
			tx.commit();
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			tx.rollback();
			throw re;
		}
	}

	public void attachDirty(SzczegolyZamowienia instance) {
		log.debug("attaching dirty SzczegolyZamowienia instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(SzczegolyZamowienia instance) {
		log.debug("attaching clean SzczegolyZamowienia instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(SzczegolyZamowienia persistentInstance) {
		log.debug("deleting SzczegolyZamowienia instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public SzczegolyZamowienia merge(SzczegolyZamowienia detachedInstance) {
		log.debug("merging SzczegolyZamowienia instance");
		try {
			SzczegolyZamowienia result = (SzczegolyZamowienia) sessionFactory.getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public SzczegolyZamowienia findById(int id) {
		log.debug("getting SzczegolyZamowienia instance with id: " + id);
		try {
			SzczegolyZamowienia instance = (SzczegolyZamowienia) sessionFactory.getCurrentSession()
					.get("magazyn.model.SzczegolyZamowienia", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<SzczegolyZamowienia> findByExample(SzczegolyZamowienia instance) {
		log.debug("finding SzczegolyZamowienia instance by example");
		try {
			List<SzczegolyZamowienia> results = (List<SzczegolyZamowienia>) sessionFactory.getCurrentSession()
					.createCriteria("magazyn.model.SzczegolyZamowienia").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
