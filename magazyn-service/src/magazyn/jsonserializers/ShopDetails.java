package magazyn.jsonserializers;

import magazyn.model.Sklepy;

public class ShopDetails{
	private int id;
	private String nazwa;
	private String adres;
	private Integer kodPocztowy;
	
	public ShopDetails(Sklepy shop){
		this.setId(shop.getId());
		this.setNazwa(shop.getNazwa());
		this.setAdres(shop.getAdres());
		this.setKodPocztowy(shop.getKodPocztowy());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getAdres() {
		return adres;
	}

	public void setAdres(String adres) {
		this.adres = adres;
	}

	public Integer getKodPocztowy() {
		return kodPocztowy;
	}

	public void setKodPocztowy(Integer kodPocztowy) {
		this.kodPocztowy = kodPocztowy;
	}
}