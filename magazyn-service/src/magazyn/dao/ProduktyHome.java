package magazyn.dao;
// Generated Dec 31, 2016 1:09:39 PM by Hibernate Tools 5.2.0.CR1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import magazyn.model.Produkty;

import magazyn.dao.GetSessionFactory;

import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class Produkty.
 * @see magazyn.model.Produkty
 * @author Hibernate Tools
 */
public class ProduktyHome {

	private static final Log log = LogFactory.getLog(ProduktyHome.class);

	private final SessionFactory sessionFactory = GetSessionFactory.getSessionFactory();
	
	private Session current_session = sessionFactory.openSession();

	public void persist(Produkty transientInstance) {
		log.debug("persisting Produkty instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Produkty instance) {
		log.debug("attaching dirty Produkty instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Produkty instance) {
		log.debug("attaching clean Produkty instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Produkty persistentInstance) {
		log.debug("deleting Produkty instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Produkty merge(Produkty detachedInstance) {
		log.debug("merging Produkty instance");
		try {
			Produkty result = (Produkty) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Produkty findById(int id) {
		log.debug("getting Produkty instance with id: " + id);
		try {
			Produkty instance = (Produkty) sessionFactory.openSession().get("magazyn.model.Produkty", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Produkty> findByExample(Produkty instance) {
		log.debug("finding Produkty instance by example");
		try {
			List<Produkty> results = (List<Produkty>) sessionFactory.getCurrentSession()
					.createCriteria("magazyn.model.Produkty").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	/**
	 * Wyszukuje produkty które zawierają podany tekst w swoich atrybutach
	 * obecnie 'opis' oraz 'nazwa' są brane pod uwagę
	 * @param fraza
	 * @return
	 */
	public int[] findByText(String fraza){
		Session current_session = sessionFactory.getCurrentSession();
		Transaction tr = current_session.beginTransaction();
		/*
		String query_text = "FROM " + Produkty.class.getSimpleName();
		Query query = current_session.createQuery(query_text);
		*/
		Criteria query = current_session.createCriteria(Produkty.class);
		query.add(Restrictions.like("nazwa", fraza, MatchMode.ANYWHERE));
		List results = query.list();
		int[] results_ids = new int[results.size()];
		for (Produkty result : (List<Produkty>) results){
			results_ids[results.indexOf(result)] = result.getId();
		}
		System.out.println(results);
		tr.commit();
		return results_ids;
	}
	
}
