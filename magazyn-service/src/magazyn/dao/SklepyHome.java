package magazyn.dao;
// Generated Dec 31, 2016 1:09:39 PM by Hibernate Tools 5.2.0.CR1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import magazyn.model.Sklepy;

import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class Sklepy.
 * @see magazyn.model.Sklepy
 * @author Hibernate Tools
 */
public class SklepyHome {

	private static final Log log = LogFactory.getLog(SklepyHome.class);

	private final SessionFactory sessionFactory = GetSessionFactory.getSessionFactory();
	private Session current_session = sessionFactory.openSession();

	public void persist(Sklepy transientInstance) {
		log.debug("persisting Sklepy instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Sklepy instance) {
		log.debug("attaching dirty Sklepy instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Sklepy instance) {
		log.debug("attaching clean Sklepy instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Sklepy persistentInstance) {
		log.debug("deleting Sklepy instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Sklepy merge(Sklepy detachedInstance) {
		log.debug("merging Sklepy instance");
		try {
			Sklepy result = (Sklepy) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Sklepy findById(int id) {
		log.debug("getting Sklepy instance with id: " + id);
		Transaction tr = current_session.beginTransaction();
		try {
			Sklepy instance = (Sklepy) current_session.get("magazyn.model.Sklepy", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			tr.commit();
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			tr.rollback();
			throw re;
		}
	}

	public List<Sklepy> findByExample(Sklepy instance) {
		log.debug("finding Sklepy instance by example");
		try {
			List<Sklepy> results = (List<Sklepy>) sessionFactory.getCurrentSession()
					.createCriteria("magazyn.model.Sklepy").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
