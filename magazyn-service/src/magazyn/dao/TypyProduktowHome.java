package magazyn.dao;
// Generated Dec 31, 2016 1:09:39 PM by Hibernate Tools 5.2.0.CR1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import magazyn.model.TypyProduktow;

import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class TypyProduktow.
 * @see magazyn.model.TypyProduktow
 * @author Hibernate Tools
 */
public class TypyProduktowHome {

	private static final Log log = LogFactory.getLog(TypyProduktowHome.class);

	private final SessionFactory sessionFactory = GetSessionFactory.getSessionFactory();
	private Session current_session = sessionFactory.openSession();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(TypyProduktow transientInstance) {
		log.debug("persisting TypyProduktow instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(TypyProduktow instance) {
		log.debug("attaching dirty TypyProduktow instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(TypyProduktow instance) {
		log.debug("attaching clean TypyProduktow instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(TypyProduktow persistentInstance) {
		log.debug("deleting TypyProduktow instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public TypyProduktow merge(TypyProduktow detachedInstance) {
		log.debug("merging TypyProduktow instance");
		try {
			TypyProduktow result = (TypyProduktow) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public TypyProduktow findById(int id) {
		log.debug("getting TypyProduktow instance with id: " + id);
		try {
			TypyProduktow instance = (TypyProduktow) sessionFactory.getCurrentSession()
					.get("magazyn.model.TypyProduktow", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<TypyProduktow> findByExample(TypyProduktow instance) {
		log.debug("finding TypyProduktow instance by example");
		try {
			List<TypyProduktow> results = (List<TypyProduktow>) sessionFactory.getCurrentSession()
					.createCriteria("magazyn.model.TypyProduktow").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
