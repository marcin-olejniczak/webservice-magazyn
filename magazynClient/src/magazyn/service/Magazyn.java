/**
 * Magazyn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package magazyn.service;

public interface Magazyn extends java.rmi.Remote {
    public int[] findProducts(java.lang.String search_query) throws java.rmi.RemoteException;
    public java.lang.String productDetails(int id) throws java.rmi.RemoteException;
    public java.lang.String ping() throws java.rmi.RemoteException;
    public int addOrder(int id_shop_user, int ids, int amounts) throws java.rmi.RemoteException;
}
