package magazyn.jsonserializers;

import java.util.Set;

import magazyn.model.Produkty;
import magazyn.model.SzczegolyZamowienia;
import magazyn.model.Zamowienia;

public class OrderDetailDetails{
	private int id;
	private ProductDetails product;
	private int amount;
	
	public OrderDetailDetails(SzczegolyZamowienia order_detail){
		this.setId(order_detail.getId());
		this.setProduct(new ProductDetails(order_detail.getProdukty()));
		this.setAmount(order_detail.getIlosc());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ProductDetails getProduct() {
		return product;
	}

	public void setProduct(ProductDetails product) {
		this.product = product;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
}
