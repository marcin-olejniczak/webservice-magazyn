package magazyn.service;

public class MagazynProxy implements magazyn.service.Magazyn {
  private String _endpoint = null;
  private magazyn.service.Magazyn magazyn = null;
  
  public MagazynProxy() {
    _initMagazynProxy();
  }
  
  public MagazynProxy(String endpoint) {
    _endpoint = endpoint;
    _initMagazynProxy();
  }
  
  private void _initMagazynProxy() {
    try {
      magazyn = (new magazyn.service.MagazynServiceLocator()).getMagazyn();
      if (magazyn != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)magazyn)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)magazyn)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (magazyn != null)
      ((javax.xml.rpc.Stub)magazyn)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public magazyn.service.Magazyn getMagazyn() {
    if (magazyn == null)
      _initMagazynProxy();
    return magazyn;
  }
  
  public int[] findProducts(java.lang.String search_query) throws java.rmi.RemoteException{
    if (magazyn == null)
      _initMagazynProxy();
    return magazyn.findProducts(search_query);
  }
  
  public java.lang.String productDetails(int id) throws java.rmi.RemoteException{
    if (magazyn == null)
      _initMagazynProxy();
    return magazyn.productDetails(id);
  }
  
  public java.lang.String ping() throws java.rmi.RemoteException{
    if (magazyn == null)
      _initMagazynProxy();
    return magazyn.ping();
  }
  
  public int addOrder(int id_shop_user, int ids, int amounts) throws java.rmi.RemoteException{
    if (magazyn == null)
      _initMagazynProxy();
    return magazyn.addOrder(id_shop_user, ids, amounts);
  }
  
  
}