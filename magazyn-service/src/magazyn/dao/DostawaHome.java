package magazyn.dao;
// Generated Dec 31, 2016 1:09:39 PM by Hibernate Tools 5.2.0.CR1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;

import magazyn.model.Dostawy;

import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class Dostawa.
 * @see magazyn.model.Dostawy
 * @author Hibernate Tools
 */
public class DostawaHome {

	private static final Log log = LogFactory.getLog(DostawaHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(Dostawy transientInstance) {
		log.debug("persisting Dostawa instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Dostawy instance) {
		log.debug("attaching dirty Dostawa instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Dostawy instance) {
		log.debug("attaching clean Dostawa instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Dostawy persistentInstance) {
		log.debug("deleting Dostawa instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Dostawy merge(Dostawy detachedInstance) {
		log.debug("merging Dostawa instance");
		try {
			Dostawy result = (Dostawy) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Dostawy findById(int id) {
		log.debug("getting Dostawa instance with id: " + id);
		try {
			Dostawy instance = (Dostawy) sessionFactory.getCurrentSession().get("magazyn.model.Dostawa", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Dostawy> findByExample(Dostawy instance) {
		log.debug("finding Dostawa instance by example");
		try {
			List<Dostawy> results = (List<Dostawy>) sessionFactory.getCurrentSession()
					.createCriteria("magazyn.model.Dostawa").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
