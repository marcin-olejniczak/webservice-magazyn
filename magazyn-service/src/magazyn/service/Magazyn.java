package magazyn.service;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import magazyn.dao.*;
import magazyn.exceptions.*;
import magazyn.model.*;
import magazyn.jsonserializers.*;

public class Magazyn {
	ProduktyHome produkty_dao = new ProduktyHome();
	ZamowieniaHome zamowienia_dao = new ZamowieniaHome();
	UzytkownicySklepowHome uzytkownicy_sklepow_dao = new UzytkownicySklepowHome();
	SklepyHome sklepy_dao = new SklepyHome();
	SzczegolyZamowieniaHome szczegoly_zamowienia_home = new SzczegolyZamowieniaHome();
	GsonBuilder builder = new GsonBuilder();
    Gson gson = builder.create();
    
	public String ping(){
		return "ok";
	}
	/**
	 * Wyszukuje produkty ktore zawieraja dany tekst w nazwie lub opisie i
	 * zwraca tablicę z id znalezionych produktów
	 * @param szukana_fraza
	 * @return tablica zawierająca id znalezionych produktow
	 */
	public int[] findProducts(String search_query){
		int[] ids = new int[]{};
		int[] products_ids = this.produkty_dao.findByText(search_query);
		return products_ids;
	}	
	/**
	 * Pobiera szczegóły produktu
	 * @param id
	 * @return zserializowany JSON
	 */
	public String productDetails(int id){
		this.gson.serializeNulls();
		Produkty product = this.produkty_dao.findById(id);
		Map response = new HashMap<String, String>();
		response.put("id", Integer.toString(product.getId()));
		response.put("name", product.getNazwa());
		response.put("description", product.getOpis());
		response.put("amount", Integer.toString(product.getIlosc()));
		response.put("prize", Integer.toString(product.getCena()));
		response.put("unit", product.getJednostkaMiary());
		return this.gson.toJson(response);
	}
	/**
	 * @param id_shop_user
	 *  id sklepu dla którego składa sie zamówienie
	 * @param ids
	 *  tablica z idkami produktow
	 * @param amounts
	 *  ilosci produktów, kolejność taka jak w 'ids'
	 * @return
	 * id dodanego produktu albo -1 w przypadku blędu
	 */
	public int addOrder(int id_shop_user, ArrayList<Integer> ids, ArrayList<Integer> amounts){
		UzytkownicySklepow shop_user = this.uzytkownicy_sklepow_dao.findById(
				id_shop_user);
		Date now = new Date();
		if(shop_user == null || ids.size() != amounts.size()){
			return -1;
		}
		Set<SzczegolyZamowienia> szczegoly_zamowienia = new HashSet<SzczegolyZamowienia>();

		Zamowienia order = new Zamowienia();
		order.setUzytkownicySklepow(shop_user);
		order.setDataModyfikacji(now);
		order.setDataUtworzenia(now);
		for(int id : ids){
			int product_index = ids.indexOf(id);
			SzczegolyZamowienia szczegol = new SzczegolyZamowienia();
			szczegol.setProdukty(this.produkty_dao.findById(id));
			szczegol.setIlosc(amounts.get(product_index));
			szczegol.setZamowienia(order);
			//this.szczegoly_zamowienia_home.persist(szczegol);
			szczegoly_zamowienia.add(szczegol);
		}
		
		order.setSzczegolyZamowienia(szczegoly_zamowienia);
		try{
			this.zamowienia_dao.persist(order);
		}catch(RuntimeException re){
			return -1;
		}
		return order.getId();
	}
	
	
	public OrderDetails orderDetails(int id) throws OrderNotFound{
		this.gson.serializeNulls();
		Zamowienia order = this.zamowienia_dao.findById(id);
		String response = "";
		// test if order exist
		if(order == null){
			throw new OrderNotFound("Order not found");
		}
		// fetch shop user
		UzytkownicySklepow shop_user = order.getUzytkownicySklepow();
		// fetch sklep
		Sklepy shop = this.sklepy_dao.findById(
				1
				);
		// fetch ZamowieniaSzczegoly
		ShopDetails shop_detail = new ShopDetails(shop);
		UserDetails user_detail = new UserDetails(shop_user.getUzytkownicy());
		
		Set<SzczegolyZamowienia> order_details = order.getSzczegolyZamowienia();
		Set<OrderDetailDetails> order_details_set = new HashSet<OrderDetailDetails>();
		for(SzczegolyZamowienia order_detail : order_details){
			order_details_set.add(
				new OrderDetailDetails(order_detail)
			);
		}
		/*
		return this.gson.toJson(
			new OrderDetails(order, user_detail, shop_detail, order_details_set));
		*/
		return new OrderDetails(order, user_detail, shop_detail, order_details_set);
	}
	
}
