/**
 * MagazynService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package magazyn.service;

public interface MagazynService extends javax.xml.rpc.Service {
    public java.lang.String getMagazynAddress();

    public magazyn.service.Magazyn getMagazyn() throws javax.xml.rpc.ServiceException;

    public magazyn.service.Magazyn getMagazyn(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
