package magazyn.jsonserializers;

import magazyn.model.Uzytkownicy;

//serialize details
public class UserDetails{
	private int id;
	private String imie;
	private String nazwisko;
	private String login;
	public UserDetails(Uzytkownicy user){
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
}

