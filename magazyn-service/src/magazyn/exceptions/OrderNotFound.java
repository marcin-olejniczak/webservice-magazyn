package magazyn.exceptions;

public class OrderNotFound extends Exception{
    public OrderNotFound(String message) {
        super(message);
    }

    public OrderNotFound(String message, Throwable throwable) {
        super(message, throwable);
    }
}
