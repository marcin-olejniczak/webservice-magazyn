package magazyn.dao;
// Generated Dec 31, 2016 1:09:39 PM by Hibernate Tools 5.2.0.CR1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import magazyn.dao.GetSessionFactory;
import magazyn.model.UzytkownicySklepow;

import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class UzytkownicySklepow.
 * @see magazyn.model.UzytkownicySklepow
 * @author Hibernate Tools
 */
public class UzytkownicySklepowHome {

	private static final Log log = LogFactory.getLog(UzytkownicySklepowHome.class);

	private final SessionFactory sessionFactory = GetSessionFactory.getSessionFactory();
	private Session current_session = sessionFactory.openSession();
	
	public void persist(UzytkownicySklepow transientInstance) {
		log.debug("persisting UzytkownicySklepow instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(UzytkownicySklepow instance) {
		log.debug("attaching dirty UzytkownicySklepow instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UzytkownicySklepow instance) {
		log.debug("attaching clean UzytkownicySklepow instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(UzytkownicySklepow persistentInstance) {
		log.debug("deleting UzytkownicySklepow instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UzytkownicySklepow merge(UzytkownicySklepow detachedInstance) {
		log.debug("merging UzytkownicySklepow instance");
		try {
			UzytkownicySklepow result = (UzytkownicySklepow) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public UzytkownicySklepow findById(int id) {
		log.debug("getting UzytkownicySklepow instance with id: " + id);
		try {
			current_session.beginTransaction();
			UzytkownicySklepow instance = (UzytkownicySklepow) current_session.get("magazyn.model.UzytkownicySklepow", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			current_session.getTransaction().commit();
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<UzytkownicySklepow> findByExample(UzytkownicySklepow instance) {
		log.debug("finding UzytkownicySklepow instance by example");
		try {
			List<UzytkownicySklepow> results = (List<UzytkownicySklepow>) sessionFactory.getCurrentSession()
					.createCriteria("magazyn.model.UzytkownicySklepow").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
